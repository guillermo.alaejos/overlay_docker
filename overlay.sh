# 1 - Los contenedores pueden comunicarse entre sí mediante protocolos IP
docker run --network=none --name=cont1 -i -t alpine
docker run --network=none --name=cont2 -i -t alpine

ip netns add nscont1
ip netns add nscont2

cont_id=testc1
cont_netns=$(docker inspect ${conta_id} --format '{{ .NetworkSettings.SandboxKey }}')
mkdir -p /var/run/netns
rm -f /var/run/netns/${cont_id}
ln -sv ${cont_netns} /var/run/netns/${cont_id}

cont_id=testc1
cont_netns=$(docker inspect ${cont_id} --format '{{ .NetworkSettings.SandboxKey }}')
mkdir -p /var/run/netns
rm -f /var/run/netns/${cont_id}
ln -sv ${cont_netns} /var/run/netns/${cont_id}

ip link add name bridge-docker type bridge
ip a a 10.0.0.1/16 dev bridge-docker
ip link set bridge-docker up

ip link add veth0 netns nscont1 type veth peer name veth0
ip link set veth0 master bridge-docker
ip link set veth1 master bridge-docker

ip netns exec nscont1 ip a a 10.0.0.2/16 dev veth0
ip netns exec nscont2 ip a a 10.0.0.3/16 dev veth0

ip netns exec nscont1 ip link set veth0 up
ip netns exec nscont2 ip link set veth0 up

ip netns exec nscont1 ip r a default via 10.0.0.1
ip netns exec nscont2 ip r a default via 10.0.0.1

# 2 - Los contenedores pueden comunicarse con el host
# Con la configuración seguida en el punto anterior, no es necesario hacer nada extra para conseguir que lo contenedores se puedan comunicar con el host

# 3 - Los contenedores pueden comunicarse con cualquier nodo
# Para hacer esto, primero es necesario que el host no tenga en la política "forward" "drop"
echo 1 > /proc/sys/net/ipv4/ip_forward
# Sin embargo, el directorio /proc está cargado en memoria y vuelve a su valor por defecto una vez se reinicie el equipo. Para solucionar esto, es posible modificar el fichero /etc/sysctl.conf añadiendo la siguiente línea:
# net.ipv4.ip_forward=1

# Ahora ya es posible añadir las reglas de nating que permitan la comunicación desde los contenedores hacia fuera 
iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE  # Se supone que la red donde se encuentran los contenedores es la subred 10.0.0.0/24 
iptables -t nat -A PREROUTING -p tcp --dport 80 -i eth0 -j DNAT --to 10.0.0.2  # Se supone que la ip del contenedor es 10.0.0.2 y que todo el tráfico dirigido al puerto 80 tiene como destino el contenedor
# Sería necesario añadir una regla de PREROUTING para cada contenedor para indicar su ip


# 4 - Comunicarse contenedores que estén en nodos diferentes
# Para este ejercicio, es necesario, primero, que los dos nodos tengan las mismas reglas de iptables.
# Una vez configurado eso, hay que añadir una regla de enroutamiento para que los paquetes que se envían al otro contendor pasen por el nodo en el que se encuentran
ip netns exec nscont1 ip r a 10.0.0.3 via 192.68.0.3  # Se supone que el segundo contenedor tiene la ip 10.0.0.3 y está alojado en un nodo con ip 192.168.0.3, y el primer contenedor tiene la ip 10.0.0.2

ip netns exec nscont2 ip r a 10.0.0.2 via 192.68.0.2  # Se supone que el primer contenedor tiene la ip 10.0.0.2 y está alojado en un nodo con ip 192.168.0.2, y el primer contenedor tiene la ip 10.0.0.3
